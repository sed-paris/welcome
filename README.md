# Welcome!

This repository hosts public informations that the SED wants to share.
Most contents are in the [wiki].

[wiki]: https://gitlab.inria.fr/sed-paris/welcome/-/wikis/home
